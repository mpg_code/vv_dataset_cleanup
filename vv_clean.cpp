#include <dirent.h>
#include <iostream>
#include <fstream>
#include <list>
#include <map>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <vector>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

static string pathSep = "/";

static bool applyChanges = false;

void msgerr(string str) {
    fprintf(stderr, "\nError: %s\n", str.c_str());
    fflush(stderr);
}

void msgerrexit(string str) {
    msgerr(str);
    exit(1);
}

void msgwarn(string str) {
    fprintf(stderr, "\nWarning: %s\n", str.c_str());
    fflush(stderr);
}

void msginfo(string str) {
    fprintf(stdout, "%s\n", str.c_str());
    fflush(stdout);
}

int system(string cmd) {
    if (cmd.size() <= 0) return 0;
    int ret = applyChanges ? system(cmd.c_str()) : 0;
    if (!applyChanges) {
        msginfo("Executing command: " + cmd);
    }
    if (WIFSIGNALED(ret) && (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))  msgerrexit("Interrupted!");
    if (!WIFEXITED(ret)) msgerrexit("Command is not finished: " + cmd);
    return WEXITSTATUS(ret);
}

bool hasBeginning (std::string const &fullString, std::string const &beginning) {
    if (fullString.length() >= beginning.length()) {
        return (0 == fullString.compare (0, beginning.size(), beginning));
    } else {
        return false;
    }
}

bool hasEnding (std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

vector<string> scandir(string path, vector<string> excludePrefix, vector<string> includeSuffix, bool directories) {
    vector<string> entries;
    struct dirent **namelist;
    int namelistlen = scandir(path.c_str(), &namelist, NULL, alphasort);
    if (namelistlen > 0) {
        for (int nameind = 0; nameind < namelistlen; nameind ++) {
            string name = namelist[nameind]->d_name;
            free(namelist[nameind]);
            if (name.size() <= 0) continue;
            if (name == "." || name == "..") continue;
            bool exclude = false;
            for (int j = 0; j < excludePrefix.size(); j ++) {
                string prefix = excludePrefix[j];
                if (hasBeginning(name,  prefix)) exclude = true;
            }
            if (exclude) continue;
            bool include = true;
            if (includeSuffix.size() > 0) {
                include = false;
                for (int j = 0; j < includeSuffix.size(); j ++) {
                    string suffix = includeSuffix[j];
                    if (hasEnding(name, suffix)) include = true;
                }
            }
            if (!include) continue;
            struct stat st;
            if (stat((path + pathSep + name).c_str(), &st)) continue;
            if ((S_ISREG(st.st_mode) && directories) || (S_ISDIR(st.st_mode) && (!directories))) continue;
            entries.push_back(name);
        }
        free(namelist);
    }
    return entries;
}

string ssprintf(const char* fmt, ...) {
    va_list args1;
    va_list args2;
    va_start (args1, fmt);
    va_copy (args2, args1);
    int buf_size = vsnprintf(NULL, 0, fmt, args1) + 1;
    char* buf = (char*)malloc(buf_size);
    vsnprintf(buf, buf_size, fmt, args2);
    va_end (args1);
    va_end (args2);
    string str = buf;
    free(buf);
    return str;
}

int main(int argc, char** argv)
{
    setlocale(LC_ALL, "C");
    for (int i = 1; i < argc; i ++) {
        if ((!strcmp(argv[i], "--apply")) || (!strcmp(argv[i], "-a"))) {
            applyChanges = true;
        }
    }
    msginfo(applyChanges ? "Changes to the files will be applied" : "No changes will be made to the files");
    vector<string> dirsList = scandir(".", vector<string> {".", "__deleted"}, vector<string>(), true);
    if (dirsList.size() <= 0) msgerrexit("Directory is empty");
    vector<string> sourceDirs;
    vector<string> referenceDirs;
    vector<string> otherDirs;
    for (int i = 0; i < dirsList.size(); i ++) {
        string dir = dirsList[i];
        //msginfo("Found directory: " + dir);
        if (dir == "Picsara") sourceDirs.push_back(dir);
        else if (dir == "picsaravideoklipp") sourceDirs.push_back(dir);
        else if (dir == "to_eval_vv_cleaned_099") referenceDirs.push_back(dir);
        else otherDirs.push_back(dir);
    }
    if (sourceDirs.size() <= 0) msgerrexit("Images sources' directories not found");
    if (referenceDirs.size() <= 0) msgerrexit("Reference classes' directories not found");
    if (otherDirs.size() <= 0) msgerrexit("No other classes' directories found");
    vector<string> dirs;
    for (string dir : referenceDirs) dirs.push_back(dir);
    for (string dir : otherDirs) dirs.push_back(dir);
    for (string dir : sourceDirs) dirs.push_back(dir);
    list<string> subdirs;
    //msginfo("Directories to process:");
    for (int i = 0; i < dirs.size(); i ++) {
        string dir = dirs[i];
        subdirs.push_back(dir);
        //msginfo("   " + dir);
    }
    //msginfo("Subdirectories to process:");
    list<string>::iterator it = subdirs.begin();
    while (it != subdirs.end()) {
        list<string>::iterator it_next = it;
        it_next ++;
        string subdir = *it;
        //msginfo("Looking into subdirectory: " + subdir);
        vector<string> subsubdirs = scandir(subdir, vector<string> {"."}, vector<string>(), true);
        for (int i = 0; i < subsubdirs.size(); i ++) {
            string subsubdir = subsubdirs[i];
            //msginfo("   " + subdir + pathSep + subsubdir);
            subdirs.insert(it_next, subdir + pathSep + subsubdir);
        }
        it ++;
    }
    //msginfo("Directories to process:");
    //for (string dir : subdirs) {
    //    msginfo("   " + dir);
    //}
    int referenceDupWarnings = 0;
    int movedFiles = 0;
    int moveErrors = 0;
    map<string,list<string>> files;
    //msginfo("Listing directory:");
    for (string dir : subdirs) {
        //msginfo("   " + dir);
        for (string file : scandir(dir, vector<string> {"."}, vector<string> {".jpg", ".jpeg", ".bmp", ".tif", ".tiff", ".png", ".avi", ".mpg", ".wmv"}, false)) {
            //msginfo("      " + file + "   [" + dir + pathSep + file + "]");
            auto it = files.find(file);
            if (it != files.end()) {
                list<string>& exdirs = it->second;
                //string msg = "File already exists in " + ssprintf("%d", (int)exdirs.size()) + " directories: " + file + " [" + dir + "] -> [";
                //for (string exdir : exdirs) {
                //    msg += (msg.back() == '[' ? "" : ",") + exdir;
                //}
                //msg += "]";
                //msginfo(msg);
                bool dupInReference = false;
                for (string etdir : referenceDirs) {
                    if (hasBeginning(dir, etdir)) {
                        dupInReference = true;
                    }
                }
                if (dupInReference) {
                    string msg = "File exists in " + ssprintf("%d", (int)exdirs.size()) + " reference directories: " + file + " [" + dir + "] -> [";
                    for (string exdir : exdirs) {
                        msg += (msg.back() == '[' ? "" : ",") + exdir;
                    }
                    msg += "]";
                    msgwarn(msg);
                    referenceDupWarnings ++;
                } else {
                    string msg = "Moving file already exists in " + ssprintf("%d", (int)exdirs.size()) + " directories: " + file + " [" + dir + "] -> [";
                    for (string exdir : exdirs) {
                        msg += (msg.back() == '[' ? "" : ",") + exdir;
                    }
                    msg += "]";
                    msginfo(msg);
                    system("mkdir -p \"__deleted" + pathSep + dir + "\"");
                    if (!system("mv -n \"" + dir + pathSep + file + "\" \"__deleted" + pathSep + dir + pathSep + file + "\"")) {
                        movedFiles ++;
                    } else {
                        moveErrors ++;
                    }
                }
                exdirs.push_back(dir);
            } else {
                files[file] = list<string> {dir};
            }
        }
    }
    msginfo("---");
    msginfo(ssprintf("Duplicates in reference directories: %d", referenceDupWarnings));
    msginfo(ssprintf("File move errors: %d", moveErrors));
    msginfo(ssprintf("Moved files: %d", movedFiles));
}
